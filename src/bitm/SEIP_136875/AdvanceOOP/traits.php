<?php
trait BigHello {
    public function sayHello() {
        echo 'Hello ';
    }
}

trait SmallWorld {
    public function sayHello() {
        echo 'hello';
    }
}

class MyHelloWorld {
    use BigHello, SmallHello
{
        BigHello::sayHello insteadof SmallHello;
        SmallHello::sayHello as tellHello;


}



    public function sayHelloWorld() {
        $this->sayHello();
        echo"<br>";
        $this->tellHello();
    }
}
$obj=new MyHelloWorld();
$obj->sayHelloWorld();